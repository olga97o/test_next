import React from 'react';
import {Provider} from "react-redux";
import axios from 'axios';

import FormComponent from "../components/FormComponent.js";
import store from "../Store.js";

axios.defaults.baseURL = 'http://test.clevertec.ru/tt';

function App() {
    return (
        <Provider store={store}>
            <FormComponent/>
            <style jsx>{`
                *{
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;

                }
            `}</style>
        </Provider>


    );
}

export default App;
